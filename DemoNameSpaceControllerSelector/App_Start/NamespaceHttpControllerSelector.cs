﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Routing;

namespace DemoNameSpaceControllerSelector
{
    public class NamespaceHttpControllerSelector : IHttpControllerSelector
    {
        private const string DefaultNamespace = "V1";
        private const string NamespaceKey = "version";
        private const string ControllerKey = "controller";

        private readonly HttpConfiguration _configuration;
        private readonly Lazy<Dictionary<string, HttpControllerDescriptor>> _controllers;
        private readonly HashSet<string> _duplicates;

        public NamespaceHttpControllerSelector(HttpConfiguration config)
        {
            _configuration = config;
            _duplicates = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            _controllers = new Lazy<Dictionary<string, HttpControllerDescriptor>>(InitializeControllerDictionary);
        }

        private Dictionary<string, HttpControllerDescriptor> InitializeControllerDictionary()
        {
            var dictionary = new Dictionary<string, HttpControllerDescriptor>(StringComparer.OrdinalIgnoreCase);

            IAssembliesResolver assembliesResolver = _configuration.Services.GetAssembliesResolver();
            IHttpControllerTypeResolver controllersResolver = _configuration.Services.GetHttpControllerTypeResolver();

            ICollection<Type> controllerTypes = controllersResolver.GetControllerTypes(assembliesResolver);

            foreach (Type t in controllerTypes)
            {
                var segments = t.Namespace.Split(Type.Delimiter);

                var controllerName = t.Name.Remove(t.Name.Length - DefaultHttpControllerSelector.ControllerSuffix.Length);

                var key = String.Format("{0}.{1}", segments[segments.Length - 1], controllerName);

                if (dictionary.Keys.Contains(key))
                {
                    _duplicates.Add(key);
                }
                else
                {
                    dictionary[key] = new HttpControllerDescriptor(_configuration, t.Name, t);
                }
            }

            foreach (string s in _duplicates)
            {
                dictionary.Remove(s);
            }
            return dictionary;
        }

        // Use IHttpRouteData to look up the values of “namespace” and “controller”. 
        // The values are stored in a dictionary as object types.
        private static T GetRouteVariable<T>(IHttpRouteData routeData, string name)
        {
            object result = null;
            if (routeData.Values.TryGetValue(name, out result))
            {
                return (T)result;
            }
            return default(T);
        }

        public HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            // Now we can use these values to find a matching controller. 
            // First, call GetRouteData to get an IHttpRouteData object from the request:
            IHttpRouteData routeData = request.GetRouteData();
            if (null == routeData)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            
            // Use this helper function to get the route values as strings:
            string nameSpace = GetRouteVariable<string>(routeData, NamespaceKey);
            if (string.IsNullOrEmpty(nameSpace))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            string controller = GetRouteVariable<string>(routeData, ControllerKey);
            if (string.IsNullOrEmpty(controller))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            // Now look for a matching controller type. 
            // For example, given “namespace” = “v1” and “controller” = “products”, 
            // this would match a controller class with the fully qualified name MyApp.Controllers.V1.ProductsController.
            // If we test the nameSpace is not the way we want, we can redirect to default version
            Regex reg = new Regex(@"v\d{1,}", RegexOptions.IgnoreCase);
            if (!reg.IsMatch(nameSpace))
            {
                nameSpace = DefaultNamespace;
            }

            string key = String.Format("{0}.{1}", nameSpace, controller);

            HttpControllerDescriptor controllerDescriptor;

            if (_controllers.Value.TryGetValue(key, out controllerDescriptor))
            {
                return controllerDescriptor;
            }
            else if ((_duplicates.Contains(key)))
            {
                throw new HttpResponseException(
                    request.CreateErrorResponse(HttpStatusCode.InternalServerError,
                        "Multiple controllers were found that match this request."));
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            return _controllers.Value;
        }
    }
}