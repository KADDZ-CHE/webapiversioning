﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DemoNameSpaceControllerSelector.Controllers.V2
{
    public class ValuesController : ApiController
    {

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "This is V2 response message : " + Guid.NewGuid());
        }

    }
}