﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DemoNameSpaceControllerSelector.Controllers.V1
{
    public class ValuesController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "This is V1 response message : " + Guid.NewGuid());
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody]UserEntity user)
        {
            return Request.CreateResponse(HttpStatusCode.OK, user.UserName);
        }
    }
    public class UserEntity
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

}