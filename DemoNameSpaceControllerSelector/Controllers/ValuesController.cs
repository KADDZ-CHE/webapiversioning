﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DemoNameSpaceControllerSelector.Controllers
{
    public class ValuesController : ApiController
    {

        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "This is default response message : " + Guid.NewGuid());
        }

    }
}