﻿using System;
using System.Collections.Generic;

namespace CodeFirstDemo
{
    public class Standard
    {
        public Standard()
        {
            StudentsList = new List<Student>();
        }
        public int StandardId { get; set; }
        public string StandardName { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Student> StudentsList { get; set; }
    }
}