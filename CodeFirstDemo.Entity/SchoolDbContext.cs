﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Reflection;

namespace CodeFirstDemo
{
    public class SchoolDbContext : DbContext
    {
        public SchoolDbContext()
            : base("DefaultConnectionString")
        {
            //Database.SetInitializer<SchoolDbContext>(new CreateDatabaseIfNotExists<SchoolDbContext>());
            //Database.SetInitializer<SchoolDbContext>(new DropCreateDatabaseAlways<SchoolDbContext>());
            //Database.SetInitializer<SchoolDbContext>(new DropCreateDatabaseIfModelChanges<SchoolDbContext>());
            //Database.SetInitializer(new SchoolDbInitializer());
        }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Standard> Standards { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentAddress> StudentAddresses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Course>()
                .HasKey(m => m.CourseId);

            modelBuilder.Entity<Student>()
                .HasKey(m => m.StudentId);

            modelBuilder.Entity<Student>()
                .HasMany(e => e.Courses)
                .WithMany()
                .Map(c =>
                {
                    c.MapLeftKey("CourseId");
                    c.MapRightKey("StudentId");
                    c.ToTable("StudentAndCourse");
                });

            modelBuilder.Entity<Standard>()
                .HasKey(m => m.StandardId);

            // one-to-many 1
            //modelBuilder.Entity<Student>().HasRequired(e => e.Standard)
            //    .WithMany(s => s.StudentsList).HasForeignKey(e => e.StdId);

            // one-to-many 2
            //modelBuilder.Entity<Standard>().HasMany(e => e.StudentsList)
            //    .WithRequired(s => s.Standard).HasForeignKey(s => s.StdId);

            modelBuilder.Entity<Standard>()
                .HasMany(s => s.StudentsList)
                .WithRequired()
                .HasForeignKey(s => s.StdId);

            // StudentAddress
            modelBuilder.Entity<StudentAddress>()
                .HasKey(e => e.StudentId);
            modelBuilder.Entity<StudentAddress>()
                .Property(e => e.StudentId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<StudentAddress>()
                .HasRequired(e => e.Student)
                .WithRequiredDependent(s => s.Address);

            base.OnModelCreating(modelBuilder);
        }
    }

    public class SchoolDbInitializer : DropCreateDatabaseAlways<SchoolDbContext>
    {
        protected override void Seed(SchoolDbContext context)
        {
            IList<Standard> defaultStandards = new List<Standard>();

            defaultStandards.Add(new Standard() { StandardName = "Standard 1", Description = "First Standard" });
            defaultStandards.Add(new Standard() { StandardName = "Standard 2", Description = "Second Standard" });
            defaultStandards.Add(new Standard() { StandardName = "Standard 3", Description = "Third Standard" });

            foreach (Standard std in defaultStandards)
                context.Standards.Add(std);

            //All standards will
            base.Seed(context);
        }
    }
}