﻿using System.Collections.Generic;

namespace CodeFirstDemo
{
    public class Course
    {
        public Course()
        {
        }

        public int CourseId { get; set; }
        public string CourseName { get; set; }
    }
}