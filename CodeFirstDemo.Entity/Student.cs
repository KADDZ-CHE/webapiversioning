﻿using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CodeFirstDemo
{
    public class Student
    {
        public Student()
        {
            Courses = new HashSet<Course>();
        }
        public int StudentId { get; set; }
        [Required]
        public string StudentName { get; set; }

        public int StdId { get; set; }
        public virtual Standard Standard { get; set; }

        [Required]
        public virtual StudentAddress Address { get; set; }

        public virtual ICollection<Course> Courses { get; set; } 
    }
}
