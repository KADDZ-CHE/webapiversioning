﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CodeFirstDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {

            InitDataBase();

            var clark = new StudentContext().GetStudent("Clark Ken");

            var firstStudent = new StudentContext().GetStudent(1);

            while (clark.IsCompleted == false || firstStudent.IsCompleted == false)
            {
                Console.Write(".");
                Thread.Sleep(100);
            }

            Task.WaitAll(clark, firstStudent);

            Console.WriteLine("clark Id is : {0}", clark.Result.StudentId);
            Console.WriteLine("first student Id is : {0}", firstStudent.Result.StudentId);

            Console.WriteLine("press any key to continue...");
            Console.ReadKey();
        }

        static void InitDataBase()
        {
            using (var ctx = new SchoolDbContext())
            {

                var zipCode = 510600;
                var prefix = 1;

                IList<Course> defautCourses = new List<Course>();

                defautCourses.Add(new Course { CourseName = "Math" });
                defautCourses.Add(new Course { CourseName = "English" });
                defautCourses.Add(new Course { CourseName = "Chinese" });

                foreach (var course in defautCourses)
                {
                    ctx.Courses.Add(course);
                }

                ctx.SaveChanges();

                IList<Student> students = new List<Student>();

                students.Add(new Student { StudentName = "Marco Savini" });
                students.Add(new Student { StudentName = "Clark Ken" });
                students.Add(new Student { StudentName = "Gian Franco" });

                foreach (var student in students)
                {
                    student.Address = new StudentAddress { Zipcode = zipCode++, Address1 = "Address 1 " + prefix, Address2 = "Address 2 " + prefix, City = "City", Country = "China", State = "GuangDong" };
                    ctx.Students.Add(student);

                    foreach (var course in defautCourses)
                    {
                        student.Courses.Add(course);
                    }

                    prefix++;
                }

                IList<Standard> defaultStandards = new List<Standard>();

                defaultStandards.Add(new Standard { StandardName = "Standard 1", Description = "First Standard" });
                defaultStandards.Add(new Standard { StandardName = "Standard 2", Description = "Second Standard" });
                defaultStandards.Add(new Standard { StandardName = "Standard 3", Description = "Third Standard" });

                foreach (var std in defaultStandards)
                {
                    ctx.Standards.Add(std);
                }

                defaultStandards[0].StudentsList.Add(students[0]);
                defaultStandards[1].StudentsList.Add(students[1]);
                defaultStandards[1].StudentsList.Add(students[2]);

                ctx.SaveChanges();



                
            }
        }
    }

    interface IStudentContext
    {
        Task<Student> GetStudent(int id);
        Task<Student> GetStudent(string name);
    }

    public class StudentContext : IStudentContext
    {
        private const int SleepTime = 2000;
        public async Task<Student> GetStudent(int id)
        {
            Student student = null;

            using (var ctx = new SchoolDbContext())
            {
                Console.WriteLine("Start Get Student By Id...");
                student = await ctx.Students.Where(s => s.StudentId == id).FirstOrDefaultAsync();
                Thread.Sleep(SleepTime);
                Console.WriteLine("\r\nFinished Get Student By Id...");
            }

            return student;
        }

        public async Task<Student> GetStudent(string name)
        {
            Student student = null;

            using (var ctx = new SchoolDbContext())
            {
                Console.WriteLine("Start Get Student By Name...");
                student = await ctx.Students.Where(s => s.StudentName == name).FirstOrDefaultAsync();
                Thread.Sleep(SleepTime * 2);
                Console.WriteLine("\r\nFinished Get Student By Name...");
            }

            return student;
        }
    }
}